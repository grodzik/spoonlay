# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit user

EGO_PN="gitlab.com/gitlab-org/gitlab-ci-multi-runner"

DESCRIPTION="Convert your Git workflow to Docker containers"
HOMEPAGE="https://github.com/harbur/captain"
SRC_URI="https://github.com/harbur/captain/releases/download/v${PV}/captain_linux_amd64"

KEYWORDS="~amd64"
LICENSE="MIT"
SLOT="0"

RESTRICT="test"

src_unpack() {
	mkdir ${S}
	cp ${DISTDIR}/captain_linux_amd64 ${S}/captain
}

src_install() {
	newbin captain captain
}
