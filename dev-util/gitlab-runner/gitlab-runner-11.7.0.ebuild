# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit user

EGO_PN="gitlab.com/gitlab-org/gitlab-ci-multi-runner"

DESCRIPTION="Official GitLab CI Runner written in Go"
HOMEPAGE="https://gitlab.com/gitlab-org/gitlab-ci-multi-runner"
SRC_URI="https://gitlab-runner-downloads.s3.amazonaws.com/v${PV}/binaries/gitlab-runner-linux-amd64 -> gitlab-runner-${PV}"

KEYWORDS="~amd64"
LICENSE="MIT"
SLOT="0"

DEPEND="dev-go/gox
	dev-go/go-bindata"

RESTRICT="test"

src_unpack() {
	mkdir ${S}
	cp ${DISTDIR}/gitlab-runner-${PV} ${S}/gitlab-runner
}

src_install() {
	newbin gitlab-runner gitlab-runner
	doinitd "${FILESDIR}"/init.d/gitlab-runner
	doconfd "${FILESDIR}"/conf.d/gitlab-runner
}

pkg_setup() {
	enewgroup gitlab-runner
	enewuser gitlab-runner -1 -1 /var/tmp/gitlab-runner/ gitlab-runner

	echo
	ewarn "To use gitlab-runner with docker, gitlab-runner user must be added"
	ewarn "to docker group:"
	ewarn
	ewarn "    gpasswd -a gitlab-runner docker"
	echo
}
