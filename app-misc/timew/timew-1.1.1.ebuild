# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit cmake-utils

DESCRIPTION="Timewarrior tracks and reports time"
HOMEPAGE="https://taskwarrior.org/docs/timewarrior/index.html"
SRC_URI="https://taskwarrior.org/download/${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~arm x86 ~x64-macos"

DEPEND="sys-libs/readline:0
	elibc_glibc? ( sys-apps/util-linux )"
RDEPEND="${DEPEND}"

src_configure() {
	mycmakeargs=(
		-DTIMEW_DOCDIR=share/doc/${P}
	)
	cmake-utils_src_configure
}
