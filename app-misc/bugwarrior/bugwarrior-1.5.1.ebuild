# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
PYTHON_COMPAT=( python3_{7,8} )
inherit distutils-r1
DESCRIPTION="Sync github, bitbucket, and trac issues with taskwarrior"
HOMEPAGE="https://github.com/ralphbean/bugwarrior"
LICENSE="GPL-3"
SLOT="0"
IUSE=""

if [ "${PV}" == "9999" ]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/ralphbean/bugwarrior"
else
	SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"
	KEYWORDS="~amd64"
fi

DEPEND="
	dev-python/setuptools[${PYTHON_USEDEP}]
	dev-python/cryptography[${PYTHON_USEDEP}]
	dev-python/future[${PYTHON_USEDEP}]
	dev-python/dogpile-cache[${PYTHON_USEDEP}]
	dev-python/python-dateutil[${PYTHON_USEDEP}]
	dev-python/taskw[${PYTHON_USEDEP}]
	dev-python/lockfile[${PYTHON_USEDEP}]"
RDEPEND=""

# dev-python/kitchen[${PYTHON_USEDEP}]
DOCS=( README.rst )
