# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit unpacker xdg-utils

MY_PN="mailspring"
DESCRIPTION="Mailspring mail client"
HOMEPAGE="https://getmailspring.com/"
SRC_URI="https://github.com/Foundry376/Mailspring/releases/download/${PV}/mailspring-${PV}-amd64.deb"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

S="${WORKDIR}"

src_install() {
	insinto /opt/mailspring
	doins -r usr
	fperms 755 /opt/mailspring/usr/bin/mailspring
	fperms 755 /opt/mailspring/usr/share/mailspring/resources/app.asar.unpacked/mailsync
	fperms 755 /opt/mailspring/usr/share/mailspring/resources/app.asar.unpacked/mailsync.bin
	dosym ../mailspring/usr/bin/mailspring /opt/bin/mailspring

	insinto /usr/share/applications
	doins usr/share/applications/mailspring.desktop

	insinto /usr/share/icons
	doins -r usr/share/icons/hicolor
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
}
