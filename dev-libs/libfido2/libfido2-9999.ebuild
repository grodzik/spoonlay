# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake-utils git-r3 toolchain-funcs

DESCRIPTION="libfido2"
HOMEPAGE="https://github.com/Yubico/libfido2"
# SRC_URI="https://github.com/Yubico/libfido2/archive/${P}.tar.gz"
EGIT_REPO_URI="https://github.com/Yubico/libfido2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ~arm ~ppc ~ppc64 x86"
IUSE=""

DEPEND="
"
RDEPEND="${DEPEND}
	dev-vcs/git
	dev-libs/libcbor
"

S=${WORKDIR}/${P}
