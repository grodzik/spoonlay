# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

: ${CMAKE_MAKEFILE_GENERATOR:=emake}

inherit cmake-utils

DESCRIPTION="libcbor"
HOMEPAGE="https://github.com/PJK/libcbor"
SRC_URI="https://github.com/PJK/libcbor/archive/v${PV}.tar.gz"
# EGIT_REPO_URI="https://github.com/PJK/libcbor"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ~arm ~ppc ~ppc64 x86"
IUSE=""

DEPEND="
"
RDEPEND="${DEPEND}
	>=dev-libs/cJSON-1.7.12
	dev-util/cmake
"

S=${WORKDIR}/${P}

src_configure() {
	local mycmakeargs=(
		-DCMAKE_BUILD_TYPE=Release
		-DCBOR_CUSTOM_ALLOC=ON
	)

	cmake-utils_src_configure
}
