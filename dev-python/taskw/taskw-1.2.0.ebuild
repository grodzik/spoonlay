# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
PYTHON_COMPAT=( python3_{7,8,9,10} )
inherit distutils-r1
DESCRIPTION="Python bindings for your taskwarrior database"
HOMEPAGE="pypi"
LICENSE="GPL-3"
SLOT="0"
IUSE=""

if [ "${PV}" == "9999" ]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/ralphbean/bugwarrior"
else
	SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"
	KEYWORDS="~amd64"
fi

DEPEND=""
RDEPEND=""
