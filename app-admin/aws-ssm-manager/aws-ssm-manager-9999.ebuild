# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit eutils rpm linux-info

DESCRIPTION="AWS Sessions Manager plugin"
HOMEPAGE="https://aws.amazon.com/systems-manager/"
SRC_URI="https://s3.amazonaws.com/session-manager-downloads/plugin/latest/linux_64bit/session-manager-plugin.rpm"
LICENSE="LGPL-3 LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
DEPEND="app-arch/rpm2targz"
S=${WORKDIR}

src_install() {
	rm -rf "$S"/etc/init
	rm -rf "$S"/etc/systemd
	mv "$S"/usr/local/sessionmanagerplugin/bin "$S/usr/"
	rm -rf "$S/usr/local"
	cp -R "$S"/* "$S/../image" -v
}
