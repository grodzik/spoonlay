# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit eutils rpm linux-info

DESCRIPTION="Sessions Manager Agent"
HOMEPAGE="https://aws.amazon.com/systems-manager/"
SRC_URI="https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm"
LICENSE="LGPL-3 LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
DEPEND="app-arch/rpm2targz"
S=${WORKDIR}

src_install() {
	rm -rf "$S"/etc/init
	rm -rf "$S"/etc/systemd
	cp -R "$S"/* "$S/../image" -v
}
