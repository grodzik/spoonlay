# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

inherit autotools git-r3 pam eutils flag-o-matic

DESCRIPTION="Uses gpg-agent to provide single sign-on"
HOMEPAGE="https://github.com/grodzik/pam-gnupg"
EGIT_REPO_URI="https://github.com/grodzik/pam-gnupg"

LICENSE="BSD-2 BSD ISC"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86 ~amd64-linux ~x86-linux"
IUSE=""

# Only supports OpenSSH via `ssh-agent` #282993
DEPEND="virtual/pam
	dev-libs/openssl:0="
RDEPEND="${DEPEND}
	app-crypt/gnupg"

DOC_CONTENTS="
	You can enable pam_ssh for system authentication by enabling
	the pam_ssh USE flag on sys-auth/pambase.
"

src_prepare() {
	default
	eautoreconf
}


src_configure() {
	econf \
		"--with-moduledir=$(getpam_mod_dir)"
}

src_install() {
	default
	prune_libtool_files --modules
}
