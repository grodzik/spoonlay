# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit autotools eutils git-r3 toolchain-funcs flag-o-matic pam

DESCRIPTION="Library for authenticating against PAM with a Yubikey"
HOMEPAGE="https://github.com/Yubico/pam-u2f"
EGIT_REPO_URI="https://github.com/Yubico/pam-u2f"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug"

RDEPEND="
	app-crypt/libu2f-host
	app-crypt/libu2f-server
	dev-libs/libfido2
"

DEPEND="${RDEPEND}
	virtual/pkgconfig"

PATCHES=( "${FILESDIR}/${PN}-1.0.2-fix-Makefile.patch" )

src_prepare() {
	default
	use debug || append-cppflags -UDEBUG_PAM -UPAM_DEBUG
	eautoreconf
}

src_configure() {
	econf --with-pam-dir=$(getpam_mod_dir)
}
